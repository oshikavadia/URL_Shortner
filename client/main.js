Template.body.onCreated(function bodyOnCreated() {
    Meteor.subscribe('urlList');
});

Template.home.events({
    'submit form'(event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        var target = event.target;
        var url = (target.url.value).trim();


        if(urlList.find({'longUrl': url}, {limit: 1}).count() > 0){
            $("#shortURL").html("http://oshi.tk/" + urlList.findOne({longUrl: url})["shortUrl"]);
            return;
            alert("after return");
        }
        /*Java String.hashcode() implementation */
        hashCode = function (str) {
            var hash = 0;
            if (str.length == 0) return hash;
            for (var i = 0; i < str.length; i++) {
                var char = str.charCodeAt(i);
                hash = ((hash << 5) - hash) + char;
                hash = hash & hash; // Convert to 32bit integer
            }
            return hash;
        }

        var URLHash = hashCode(url);

        var URLHex = URLHash.toString(16);
        if (URLHex.charAt(0) === '-') {
            URLHex = URLHex.substr(1, URLHex.length);
        }

        var dbObj = {
            "longUrl": url,
            "shortUrl": URLHex
        };

        urlList.insert(dbObj);

        $("#shortURL").html("http://oshi.tk/" + URLHex);

    },
});