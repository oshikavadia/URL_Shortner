urlList = new Mongo.Collection('url');
if (Meteor.isServer) {
    // This code only runs on the server
    Meteor.publish('urlList', function urlListPublication() {
        return urlList.find();
    });
}
